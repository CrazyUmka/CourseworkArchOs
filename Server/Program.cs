﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace Server
{
    class ServerForAgent
    {
        private static bool genStart = false;
        private static Queue<string> queueTasks = new Queue<string>();
        private static Mutex mtx = new Mutex();
        private static Mutex mtxAgn = new Mutex();
        private Random rdm = new Random();
        private TcpListener listen;

        public ServerForAgent(string address="127.0.0.1", int port=5000)
        {
            IPAddress ipAdrs;
            if (IPAddress.TryParse(address, out ipAdrs))
            {
                listen = new TcpListener(ipAdrs, port);
                Console.Write("Launch generator tasks: ");
                genStart = true;
                Thread t = new Thread(generateTasks);
                t.Start();
                Console.WriteLine("OK.\nReady... Wait connections agent...\n");
                ListenClients();
            }
            else
            {
                Console.WriteLine("Wrong Address Server.\n\tPlease enter example: 127.0.0.1");
            }
        }
        private void ListenClients()
        {
            listen.Start();
            while (genStart)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(workerClient), listen.AcceptTcpClient());
            }
        }
        private static void workerClient(Object StateInfoClient)
        {
            TcpClient cli = (TcpClient)StateInfoClient;
            NetworkStream streamServer = cli.GetStream();
            try
            {
                Console.WriteLine("\rConnect agent accept.");
                SendTaskToAgent(streamServer);
            }
            catch (Exception e)
            {
                mtxAgn.WaitOne();
                Console.WriteLine(e.Message);
                mtxAgn.ReleaseMutex();
                cli.Close();
            }
        }

        private static void SendTaskToAgent(NetworkStream streamServer)
        {
            int countReadSymbols;
            string encMsg;
            byte[] bufferRead = new byte[1024];
            mtxAgn.WaitOne();
            byte[] bufferWrite = Encoding.ASCII.GetBytes(queueTasks.Dequeue());
            mtxAgn.ReleaseMutex();
            streamServer.Write(bufferWrite, 0, bufferWrite.Length);
            while ((countReadSymbols = streamServer.Read(bufferRead, 0, bufferRead.Length)) > 0)
            {
                encMsg = Encoding.ASCII.GetString(bufferRead, 0, countReadSymbols);
                Dictionary<string, int> resultTask = JsonConvert.DeserializeObject<Dictionary<string, int>>(encMsg);
                mtxAgn.WaitOne();
                Console.WriteLine(String.Format("Result Task: {0}\n\tExecute time: {1}", resultTask["result"], resultTask["delay"]));
                bufferWrite = Encoding.ASCII.GetBytes(queueTasks.Dequeue());
                mtxAgn.ReleaseMutex();
                streamServer.Write(bufferWrite, 0, bufferWrite.Length);
            }
        }
        private void generateTasks()
        {
            while (genStart)
            {
                Dictionary<string, int> genTask = new Dictionary<string, int>();
                genTask.Add("id_task", rdm.Next(0, 3));
                genTask.Add("arg1", rdm.Next(1, 10000));
                genTask.Add("arg2", rdm.Next(1, 10000));
                genTask.Add("delay", rdm.Next(0, 2000));
                string task = JsonConvert.SerializeObject(genTask);
                if (queueTasks.Count < 3000000)
                    queueTasks.Enqueue(task);
                else
                    Thread.Sleep(35000);
            }
        }

        ~ServerForAgent()
        {
            genStart = false;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(String.Format("PROCESS AGENT. VERSION: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()));
            Console.WriteLine("Hi, I am launching. Please wait...\n");
            int maxThread = Environment.ProcessorCount * 4;
            ThreadPool.SetMaxThreads(maxThread, maxThread);
            ThreadPool.SetMinThreads(2, 2);
            if (args.Length > 0 && args.Length == 2)
            {
                new ServerForAgent(args[0], Convert.ToInt32(args[1]));
            }
            else
            {
                new ServerForAgent();
            }
        }
    }
}
