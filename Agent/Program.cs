﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;


namespace Agent
{
    class Agent
    {
        public Agent(string address="127.0.0.1", int port=5000)
        {
            try
            {
                TcpClient clientSrv = new TcpClient(address, port);
                if (clientSrv.Connected)
                {
                    Console.WriteLine("\tAgent connect: SUCCESS");
                    linkToServer(clientSrv.GetStream());
                }
                else
                    Console.WriteLine("Agent not connected server");
                clientSrv.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        private Dictionary<string, int> formResult(int resultTask, int delay)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            result.Add("result", resultTask);
            result.Add("delay", delay);
            return result;
        }
        private int executeTask(Dictionary<string, int> task)
        {
            int idTsk = task["id_task"];
            Thread.Sleep(task["delay"]);

            if (idTsk == 0)
                return task["arg1"] + task["arg2"];
            else if (idTsk == 1)
                return task["arg1"] - task["arg2"];
            else if (idTsk == 2)
                return task["arg1"] * task["arg2"];
            else if (idTsk == 3)
                return task["arg1"] / task["arg2"];
            return 0;
        }
        private void linkToServer(NetworkStream streamServer)
        {
            int countReadSymbols;
            int countReceiveTask = 0;
            byte[] bufferRead = new byte[1024];
            string msg;
            while ((countReadSymbols = streamServer.Read(bufferRead, 0, bufferRead.Length)) > 0)
            {
                countReceiveTask++;
                msg = Encoding.ASCII.GetString(bufferRead, 0, countReadSymbols);
                Console.WriteLine(String.Format("\rReceive task: {0}\n\tJsonMsg:{1}", countReceiveTask, msg));
                Dictionary<string, int> task = JsonConvert.DeserializeObject<Dictionary<string, int>>(msg);
                Dictionary<string, int> formRes = formResult(executeTask(task), task["delay"]);
                byte[] encMsg = encMsgToServer(formRes);
                streamServer.Write(encMsg, 0, encMsg.Length);
            }
            streamServer.Close();
        }
        private byte[] encMsgToServer(Dictionary<string, int> formRes)
        {
            return Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(formRes));
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(String.Format("PROCESS AGENT. VERSION: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()));
            Console.WriteLine("Hi, I am trying connect server. Please wait...\n");
            if (args.Length > 0 && args.Length == 2)
            {
                new Agent(args[0], Convert.ToInt32(args[1]));
            }
            else
            {
                new Agent();
            }
            Console.WriteLine("\nPlease enter any key to exit...");
            Console.ReadKey();
        }
    }
}
